package com.adaptavist.projectconfigurator.ssscextension;

import com.atlassian.plugin.spring.scanner.annotation.Profile;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.awnaba.projectconfigurator.extensionpoints.common.HookPointCollection;
import com.awnaba.projectconfigurator.extensionpoints.extensionservices.ReferenceMarkerFactory;
import com.awnaba.projectconfigurator.extensionpoints.extensionservices.TranslatorFactory;
import com.awnaba.projectconfigurator.extensionpoints.gadget.GadgetTranslationPoint;
import com.awnaba.projectconfigurator.extensionpoints.gadget.GadgetTranslationPointImpl;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Profile("pc4j-extensions")
@Component
public class SSSCExtensionModule implements HookPointCollection {

	private TranslatorFactory translatorFactory;
	private ReferenceMarkerFactory referenceMarkerFactory;

	@Inject
	public SSSCExtensionModule(@ComponentImport TranslatorFactory translatorFactory,
								@ComponentImport ReferenceMarkerFactory referenceMarkerFactory) {
		this.translatorFactory = translatorFactory;
		this.referenceMarkerFactory = referenceMarkerFactory;
	}

	public GadgetTranslationPoint getSSSCGadgetFilterHookPoint() {

		return new GadgetTranslationPointImpl.Builder().
				withTranslator(translatorFactory.fromOption(TranslatorFactory.TranslateOption.FILTER_ID)).
				withUri("rest/gadgets/1.0/g/com.ja.jira.plugin.searchrequest:sswc-gadget/gadget_wc.xml").
				withParamKey("filterId").build();
	}


}
